import Vue from 'vue';
import { BootstrapVue } from 'bootstrap-vue';
import store from './vue/store';
/*
** start bootstrap
*/
// import { PaginationPlugin, FormSelectPlugin, ProgressPlugin, CollapsePlugin, VBTogglePlugin } from 'bootstrap-vue'
// import { BCard } from 'bootstrap-vue';
/*
** end bootstrap
*/
import CardExample from './vue/CardExample.vue';

/*
** start bootstrap
*/
Vue.use(BootstrapVue); // entire BS Vue package
// Vue.use(PaginationPlugin)
// Vue.use(FormSelectPlugin)
// Vue.use(ProgressPlugin)
// Vue.use(CollapsePlugin)
// Vue.use(VBTogglePlugin)
// Vue.component('b-card', BCard);
/*
** end bootstrap
*/
//
// PDP
const CardExampleEl = document.getElementById('card-example');

// Card Example
if (CardExampleEl != null) {
	new Vue({ // eslint-disable-line no-new
		el: '#card-example',
		store,
		render(h) {
			return h(CardExample);
		},
	});
}

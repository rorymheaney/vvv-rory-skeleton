/* eslint-disable no-undef */
function resizeWidthOnly(start, finish) {
	const initialInnerWidth = [window.innerWidth];
	// eslint-disable-next-line no-return-assign, func-names, no-restricted-globals
	return onresize = function () {
		const newInnerWidth = window.innerWidth;
		const previousInnerWidth = initialInnerWidth.length;
		initialInnerWidth.push(newInnerWidth);
		if (initialInnerWidth[previousInnerWidth] !== initialInnerWidth[previousInnerWidth - 1]) {
			clearTimeout(finish);
			finish = setTimeout(start, 100);
		}
	// eslint-disable-next-line no-sequences
	}, start;
}

function returnBootstrapBreakpoints() {
	const mediaQueryXXL = window.matchMedia('(min-width: 1400px)');
	const mediaQueryXL = window.matchMedia('(min-width: 1200px) and (max-width: 1399px)');
	const mediaQueryL = window.matchMedia('(min-width: 1024px) and (max-width: 1199px)');
	const mediaQueryM = window.matchMedia('(min-width: 768px) and (max-width: 1023px)');

	// Check if the media query is true, set to match bs media query variables
	if (mediaQueryXXL.matches) {
		return 'xxl';
	} if (mediaQueryXL.matches) {
		return 'xl';
	} if (mediaQueryL.matches) {
		return 'lg';
	} if (mediaQueryM.matches) {
		return 'md';
	}
	return 'sm';
}

// bs modal didn't support shift + tab by default
// it would get stuck on the modal, it's a known "issue"
function trapFocus(modal) {
	const focusableEls = modal.find('a[href]:not([disabled]), button:not([disabled]), textarea:not([disabled]), input[type="text"]:not([disabled]), input[type="radio"]:not([disabled]), input[type="checkbox"]:not([disabled]), select:not([disabled]), iframe').filter(':visible');
	const firstFocusableEl = focusableEls[0];
	const lastFocusableEl = focusableEls[focusableEls.length - 1];
	const KEYCODE_TAB = 9;
	// console.log(firstFocusableEl);
	// console.log(lastFocusableEl);
	modal.on('keydown', (e) => {
		const isTabPressed = (e.key === 'Tab' || e.keyCode === KEYCODE_TAB);
		if (!isTabPressed) {
			return;
		}
		// console.log(e.keyCode);
		if (e.shiftKey) /* shift + tab */ {
			if (document.activeElement === firstFocusableEl) {
				lastFocusableEl.focus();
				e.preventDefault();
			}
		} else /* tab */ if (document.activeElement === lastFocusableEl) {
			firstFocusableEl.focus();
			e.preventDefault();
		}
	});
}

// bootstrap carousel ADA updates
function accessibleCarousel(currentCarousel) {
	const bsCarousel = document.getElementById(currentCarousel);
	const bsSlides = bsCarousel.querySelectorAll('.carousel-item');
	const bsIndicators = bsCarousel.querySelectorAll('[data-js="update-indicator-text"]');
	const currentIndicator = 'current slide is';
	const playPause = bsCarousel.querySelector('[data-js="pause-play-carousel"]');
	const playPauseIcon = bsCarousel.querySelector('[data-js="play-pause-icon"]');
	const carouselMethods = new bootstrap.Carousel(bsCarousel, {
		ride: 'carousel',
		interval: 5000,
		pause: false,
	});

	// update carousel slides and indicators on slide
	bsCarousel.addEventListener('slide.bs.carousel', (event) => {
		const currentSlide = event.to;
		// reset all slides
		bsSlides.forEach((el) => {
			el.setAttribute('aria-hidden', 'true');
			el.setAttribute('tabindex', '-1');
		});
		// updated current slide
		bsSlides[currentSlide].setAttribute('aria-hidden', 'false');
		bsSlides[currentSlide].removeAttribute('tabindex');
		// reset all indicators if using them, if not remove this
		if (bsIndicators) {
			bsIndicators.forEach((el) => {
				el.innerHTML = '';
			});
			// // update current indicator ( pagination )
			bsIndicators[currentSlide].innerHTML = currentIndicator;
		}
	});

	// pause the carousel and update button
	function carouselPause() {
		carouselMethods.pause();
		playPauseIcon.classList.add('bi-pause-fill');
		playPauseIcon.classList.remove('bi-play-fill');
		playPause.setAttribute('aria-label', 'play carousel');
	}

	// play carousel and update button
	function carouselPlay() {
		carouselMethods.cycle();
		playPauseIcon.classList.add('bi-play-fill');
		playPauseIcon.classList.remove('bi-pause-fill');
		playPause.setAttribute('aria-label', 'pause carousel');
	}

	// listen for play pause event change
	playPause.addEventListener('click', (event) => {
		event.preventDefault();
		const playPauseLabel = playPause.getAttribute('aria-label');
		if (playPauseLabel === 'play carousel') {
			carouselPlay();
		} else {
			carouselPause();
		}
	}, false);

	// pause on hover, we do this because default pause hover re-activates automatically
	// this is an issue when it should be paused by user choice
	$(bsCarousel).on({
		mouseenter() {
			carouselPause();
		},
		mouseleave() {
			carouselPlay();
		}
	});
}

function playPauseAosAnimation(AOS, cb) {
	let clicked = false;
	const $animationToggle = $('[data-js="play-pause-aos"]');
	const $aosAttrs = $('[data-aos]');
	const animationClass = 'aos-animate';
	const $pauseText = $('[data-js="toggle-aos-pause-text"]');
	const $playText = $('[data-js="toggle-aos-play-text"]');
	const hideText = 'd-none';

	$animationToggle.on('click', () => {
		if (clicked) {
			$aosAttrs.removeClass(animationClass);
			// will reset all the aos elements and allow you to view them once again
			AOS.refreshHard();
			clicked = false;
		} else {
			// another function passed?
			$aosAttrs.addClass(animationClass);
			clicked = true;
		}
		$pauseText.toggleClass(hideText);
		$playText.toggleClass(hideText);
		// another function passed?
		// example, see home page,
		// passes click function to pause carousel when all animations paused etc
		if (cb) {
			cb();
		}
	});
}

export {
	returnBootstrapBreakpoints,
	resizeWidthOnly,
	trapFocus,
	accessibleCarousel,
	playPauseAosAnimation
};

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="preload" href="<?= get_template_directory_uri(); ?>/public/vendor/jquery-migrate-3.2.0.min.js?ver=3.2.0" as="script">
	<link rel="preload" href="<?= get_template_directory_uri(); ?>/public/vendor/jquery-3.4.1.min.js?ver=3.4.1" as="script">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
	<?php wp_head(); ?>

	<?php
		// if ie 11, load the following:
		// polyfill for BS 5 css/js
		// polyfill for vue
		// ie11 js only js?>
	<script nomodule crossorigin="anonymous"
    	src="https://polyfill.io/v3/polyfill.min.js?features=Object.assign%2CPromise%2CPromise.prototype.finally%2Cdefault%2CArray.prototype.includes%2CArray.prototype.find%2CNumber.parseFloat%2CNumber.parseInt%2CArray.prototype.forEach%2CNodeList.prototype.forEach">
    </script>
	<script nomodule>
			window.MSInputMethodContext && document.documentMode && document.write('<link src="<?= get_template_directory_uri()?>/public/vendor/bootstrap-ie11.min.css"><script src="<?= get_template_directory_uri()?>/public/vendor/bootstrap-ie11.js"><\/script><script src="https://cdn.jsdelivr.net/combine/npm/ie11-custom-properties@4,npm/element-qsa-scope@1"><\/script>');
	</script>

</head>

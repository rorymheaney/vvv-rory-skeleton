module.exports = {
	plugins: {
		'postcss-preset-env': {
			stage: 0,
			browsers: ['last 2 versions', 'not dead', '> 0.2%']
		},
	}
};

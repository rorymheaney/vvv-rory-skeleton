module.exports = {
	test: /\.vue$/,
	// loader: 'vue-loader',
	use: [
		'vue-loader',
		'eslint-loader',
	],
};

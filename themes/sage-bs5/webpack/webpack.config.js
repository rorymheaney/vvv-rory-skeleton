const path = require('path');
const isdev = require('isdev');
const webpack = require('webpack');
// const autoprefixer = require('autoprefixer');

const CopyPlugin = require('copy-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
// const StyleLintPlugin = require('stylelint-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
// const { default: ImageminPlugin } = require('imagemin-webpack-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');
// const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
// eslint-disable-next-line import/no-extraneous-dependencies
const TerserPlugin = require('terser-webpack-plugin');
// const chokidar = require('chokidar');
const sassRule = require('./rules/sass');
const fontsRule = require('./rules/fonts');
const imagesRule = require('./rules/images');
const javascriptRule = require('./rules/javascript');
const svgSpriteRule = require('./rules/svg.sprites');
const vuecss = require('./rules/vuecss');
const vuescss = require('./rules/vuescss');
const vuejs = require('./rules/vuejs');
const externalFontsRule = require('./rules/external.fonts');
const externalImagesRule = require('./rules/external.images');
// const JavaScriptObfuscator = require('webpack-obfuscator')
const config = require('./app.config');

const MODE_BEING_RUN = process.env.npm_lifecycle_event;

module.exports = {
	/**
	 * Should the source map be generated?
	 * @type {string|undefined}
	 */
	devtool: isdev && config.settings.sourceMaps ? 'cheap-module-source-map' : undefined,

	/**
	 * Application entry files for building.
	 * @type {Object}
	 */
	entry: config.assets,

	target: ['web', 'es5'],

	/**
	 * Output settings for application scripts.
	 * @type {Object}
	 */
	output: {
		path: config.paths.public,
		filename: config.outputs.javascript.filename,
	},

	/**
	 * split js for production js optimimzation
	 * @type {Object}
	 */
	/**
        ,
    */
	optimization: {
		nodeEnv: 'production',
		// minimize: true,
		minimizer: [
			// eslint-disable-next-line no-undef
			new TerserPlugin({
				test: /\.js(\?.*)?$/i,
				parallel: true
			}),
		],
	},

	cache: {
		type: 'memory'
		// type: 'filesystem',
		// cacheLocation: path.resolve(__dirname, '.test_cache'),
	},

	/**
	 * External objects which should be accessible inside application scripts.
	 * @type {Object}
	 */
	externals: config.externals,

	/**
	 * Performance settings to speed up build times.
	 * @type {Object}
	 */
	performance: {
		hints: false,
	},

	/**
	 * Build rules to handle application assset files.
	 * @type {Object}
	 */
	module: {
		rules: [
			vuecss,
			vuescss,
			vuejs,
			sassRule,
			fontsRule,
			imagesRule,
			javascriptRule,
			externalFontsRule,
			externalImagesRule,
			svgSpriteRule,
		],
	},

	/**
	 * Vue integration
	 */
	resolve: {
		alias: {
			vue$: 'vue/dist/vue.esm.js',
			'@': path.resolve(__dirname, 'assets'),
		},
	},

	/**
	 * Common plugins which should run on every build.
	 * @type {Array}
	 */
	plugins: [
		new CleanWebpackPlugin({
			default: config.paths.root,
			cleanOnceBeforeBuildPatterns: config.paths.public,
		}),
		new VueLoaderPlugin(),
		new webpack.LoaderOptionsPlugin({
			minimize: !isdev,
			// postcss: {}
		}),
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
		}),
		// new ExtractTextPlugin(config.outputs.css),
		new MiniCssExtractPlugin({
			// Options similar to the same options in webpackOptions.output
			// both options are optional
			filename: '../public/styles/[name].css',
		}),
		// ESLint configuration
		new ESLintPlugin({
			formatter: 'table',
		}),
		// new StyleLintPlugin(),
		new CopyPlugin({
			patterns: [
				{
					from: config.paths.images,
					to: config.outputs.image.filename,
				},
				{
					from: config.paths.webfonts,
					to: config.outputs.webfont.filename,
				},
				{
					from: config.paths.vendor,
					to: config.outputs.vendor,
				},
				// {
				// from: config.paths.temps,
				// to: config.outputs.temp.filename,
				// },
			],
		}),
		new BrowserSyncPlugin(
			{
				host: 'localhost',
				proxy: 'http://bootstrap-five.wordpress.test/',
				files: [
					'public/**/*.css',
					{
						match: [
							'*.php',
							'templates/*.php',
							'templates/**/*.php',
							'templates/**/**/*.php',
							'partials/**/*.php',
							'lib/**/*.php',
							'public/**/*.js'
						],
						// fn: (event, file) => {
						// if (event === 'change') {
						// // eslint-disable-next-line global-require
						// const bs = require('browser-sync').get('bs-webpack-plugin');
						// if (file.split('.').pop() === 'js' || file.split('.').pop() === 'php') {
						// bs.reload();
						// }
						// }
						// }
					},
				],
				reloadDelay: 250
			},
			{
				injectChanges: true,
				injectCss: true,
				// reload: false
			}
		),
	],
};

if (MODE_BEING_RUN === 'production') {
	module.exports.plugins.push(
		new OptimizeCSSAssetsPlugin({
			cssProcessorOptions: { discardComments: { removeAll: true } },
		}),
		// new ImageminPlugin({
		// test: /\.(jpe?g|png|gif|svg)$/i,
		// optipng: { optimizationLevel: 7 },
		// gifsicle: { optimizationLevel: 3 },
		// pngquant: { quality: '65-90', speed: 4 },
		// svgo: { removeUnknownsAndDefaults: false, cleanupIDs: false },
		// })
	);
	// Object.assign(module.exports.optimization,{
	//     splitChunks: {
	//         cacheGroups: {
	//             commons: {
	//                 name: 'commons',
	//                 chunks: 'initial',
	//                 minChunks: 2
	//             }
	//         }
	//     },
	//     runtimeChunk: true
	// })
} else {
	// module.exports.plugins.push(
	// new webpack.HotModuleReplacementPlugin(),
	// );
}
